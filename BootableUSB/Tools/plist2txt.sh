#! /bin/bash

plistutil -i $1 -o tmp.xml
awk '/ShadowHashData/ {p=1}; p; /<\/data>/ {p=0}' tmp.xml > tmp.txt
rm tmp.xml
tail -n +4 tmp.txt > tmp2.txt
cat tmp2.txt | tac | tail -n +2 | tac > shadow.txt
rm tmp2.txt tmp.txt
cat shadow.txt | tr -d \\n | tr -d \\t | base64 -d > shadowhash
rm shadow.txt
plistutil -i shadowhash -o shadowhash.xml
sed -n '/<key>SALTED-SHA512-PBKDF2<\/key>/,$p' shadowhash.xml > shadowhash2.xml
awk '/<key>entropy<\/key>/ {p=1}; p; /<\/data>/ {p=0}' shadowhash2.xml > entropy
awk '/<integer>/ {p=1}; p; /<\/integer>/ {p=0}' shadowhash2.xml > iterations
awk '/<key>salt<\/key>/ {p=1}; p; /<\/data>/ {p=0}' shadowhash2.xml > salt
rm shadowhash shadowhash.xml shadowhash2.xml

tail -n +3 entropy > entropy2
cat entropy2 | tac | tail -n +2 | tac > entropy
rm entropy2

cut -c12-16 iterations > iterations2
cat iterations2 > iterations
rm iterations2

tail -n +3 salt > salt2
head -n 1 salt2 > salt
rm salt2

cat entropy | tr -d \\n | tr -d \\t | base64 -d > entropy.txt
cat salt | tr -d \\n | tr -d \\t | base64 -d > salt.txt

xxd -p entropy.txt | tr -d \\n > entropy
xxd -p salt.txt | tr -d \\n > salt
rm entropy.txt
rm salt.txt

TMP="\$ml$"$(cat iterations)"\$"$(cat salt)"\$"$(cat entropy)
echo $TMP > $1.hash
rm salt
rm entropy
rm iterations
