int ledPin = 13;
int dt = 500;

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  delay(dt*5);
  key(MODIFIERKEY_GUI, KEY_SPACE);
  typeIn("terminal");
  delay(dt);
  typeIn("Hello world =");<<

void loop() {
  digitalWrite(ledPin, HIGH);
  delay(100);
  digitalWrite(ledPin, LOW);
  delay(100);
}


void typeIn(String cmd){
  Keyboard.print(cmd);
  delay(dt);
  Keyboard.println("");
  delay(dt);
}

void type(String cmd){
  Keyboard.print(cmd);
  delay(dt);
}

void key(int MODIFIER, int KEY){
  Keyboard.press(MODIFIER);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER){
  Keyboard.press(MODIFIER);
  Keyboard.releaseAll();
  delay(dt);
}

