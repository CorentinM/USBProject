from time import sleep
import sys

def return_chr(char):
    if(char=='q'):return 4
    elif(char=='b'):return 5
    elif(char=='c'):return 6
    elif(char=='d'):return 7
    elif(char=='e'):return 8
    elif(char=='f'):return 9
    elif(char=='g'):return 10
    elif(char=='h'):return 11
    elif(char=='i'):return 12
    elif(char=='j'):return 13
    elif(char=='k'):return 14
    elif(char=='l'):return 15
    elif(char=='n'):return 17
    elif(char=='o'):return 18
    elif(char=='p'):return 19
    elif(char=='a'):return 20
    elif(char=='r'):return 21
    elif(char=='s'):return 22
    elif(char=='t'):return 23
    elif(char=='u'):return 24
    elif(char=='v'):return 25
    elif(char=='z'):return 26
    elif(char=='x'):return 27
    elif(char=='y'):return 28
    elif(char=='w'):return 29
    elif(char==','):return 16
    elif(char=='&'):return 30
    elif(char=='('):return 34
    elif(char=='-'):return 35
    elif(char=='_'):return 37
    elif(char==')'):return 45
    elif(char=='='):return 46
    elif(char=='m'):return 51
    elif(char==';'):return 54
    elif(char==':'):return 55
    elif(char=='!'):return 56
    elif(char==' '):return 44
    elif(char=='$'):return 48
    elif(char=='*'):return 49
    elif(char=='%'):return 52
    elif(char=='<'):return 100
    elif(char=='/'):return 84
    elif(char=='1'):return 30
    elif(char=='2'):return 31
    elif(char=='3'):return 32
    elif(char=='4'):return 33
    elif(char=='5'):return 34
    elif(char=='6'):return 35
    elif(char=='7'):return 36
    elif(char=='8'):return 37
    elif(char=='9'):return 38
    elif(char=='0'):return 39

NULL_CHAR = chr(0);

def write_report(report):
    with open('/dev/hidg0', 'rb+') as fd:
        fd.write(report.encode())

with open(sys.argv[1]) as f:
    content = f.read().splitlines()

for line in content:
    print line
    words = line.split()
    if words[0]=='ENTER':
        write_report(NULL_CHAR*2+chr(40)+NULL_CHAR*5)
        write_report(NULL_CHAR*8)
    elif words[0]=='DELAY':
        sleep(int(words[1])/1000)
    elif (words[0]=='GUI' or words[0]=='WINDOWS'):
        if (line=='GUI' or line=='WINDOWS'):
            write_report(chr(8)+NULL_CHAR*7)
            write_report(NULL_CHAR*8)
        elif (words[1]=='SPACE'):
            write_report(chr(8)+NULL_CHAR+chr(44)+NULL_CHAR*5)
            write_report(NULL_CHAR*8)
        else:
            nb = return_chr(words[1][0])
            write_report(chr(8)+NULL_CHAR+chr(nb)+NULL_CHAR*5)
            write_report(NULL_CHAR*8)
    elif words[0]=='F1':
        write_report(NULL_CHAR*2+chr(58)+NULL_CHAR*5)
        write_report(NULL_CHAR*8)
    elif words[0]=='ALT':
        if words[1]=='F2':
            write_report(chr(4)+NULL_CHAR+chr(59)+NULL_CHAR*5)
            write_report(NULL_CHAR*8)
        elif words[1]=='F4':
            write_report(chr(4)+NULL_CHAR+chr(61)+NULL_CHAR*5)
            write_report(NULL_CHAR*8)
        elif (words[1]=='SPACE'):
            write_report(chr(4)+NULL_CHAR+chr(44)+NULL_CHAR*5)
            write_report(NULL_CHAR*8)
        else:
            nb = return_chr(words[1][0])
            write_report(chr(4)+NULL_CHAR+chr(nb)+NULL_CHAR*5)
            write_report(NULL_CHAR*8)
    elif words[0]=='CTRL':
        nb = return_chr(words[1][0])
        write_report(chr(1)+NULL_CHAR+chr(nb)+NULL_CHAR*5)
        write_report(NULL_CHAR*8)
    elif words[0]=='STRING':
        text=line[7:]
        for char in text:
            if ((char >= 'A' and char <= 'Z') or char=='%'):
                char = char.lower()
                nb = return_chr(char)
                write_report(chr(2)+NULL_CHAR+chr(nb)+NULL_CHAR*5)
                write_report(NULL_CHAR*8)
            elif (char >= '0' and char <= '9'):
                nb = return_chr(char)
                write_report(chr(2)+NULL_CHAR*2+chr(nb)+NULL_CHAR*4)
                write_report(NULL_CHAR*8)
            elif char=='<':
                nb = return_chr(char)
                write_report(NULL_CHAR*2+chr(nb)+NULL_CHAR*5)
                write_report(NULL_CHAR*8)
            elif char=='>':
                nb = return_chr('<')
                write_report(chr(2)+NULL_CHAR+chr(nb)+NULL_CHAR*5)
                write_report(NULL_CHAR*8)
            elif char=='.':
                nb = return_chr(';')
                write_report(chr(2)+NULL_CHAR+chr(nb)+NULL_CHAR*5)
                write_report(NULL_CHAR*8)
            else:
                nb = return_chr(char)
                write_report(NULL_CHAR*2+chr(nb)+NULL_CHAR*5)
                write_report(NULL_CHAR*8)
    elif (words[0]=='CAPS' or words[0]=='CAPSLOCK'):
        write_report(NULL_CHAR*2+chr(57)+NULL_CHAR*5)
        write_report(NULL_CHAR*8)
    elif words[0]=='RIGHTARROW':
        write_report(NULL_CHAR*2+chr(79)+NULL_CHAR*5)
        write_report(NULL_CHAR*8)
    elif words[0]=='LEFTARROW':
        write_report(NULL_CHAR*2+chr(80)+NULL_CHAR*5)
        write_report(NULL_CHAR*8)
    elif words[0]=='DOWNARROW':
        write_report(NULL_CHAR*2+chr(81)+NULL_CHAR*5)
        write_report(NULL_CHAR*8)
    elif words[0]=='UPARROW':
        write_report(NULL_CHAR*2+chr(82)+NULL_CHAR*5)
        write_report(NULL_CHAR*8)
    elif words[0]=='NUMLOCK':
        write_report(NULL_CHAR*2+chr(83)+NULL_CHAR*5)
        write_report(NULL_CHAR*8)
