import sys
import itertools
import os

def month(m):
    if m=="01":return ["janvier","Janvier", "january", "January"]
    if m=="02":return ["fevrier","Fevrier", "february", "February"]
    

if (len(sys.argv)!=2):
	print("Usage : python WordGen.py List.txt")
	print("List.txt contains entries in this form : <Type>:<Value>")
	print("<Type> can be Name, Date (DDMMAAAA), or Other.")
	print("Names and Dates will be processed deeply, Others will be taken as it comes")

else:
	with open(sys.argv[1]) as f:
		content = f.read().splitlines()

	file = open("Passwords.txt","w")
	total = []
	cmplx = input("Complexity (1, 2, 3 or 4). It corresponds to the level of processing of each entry of the input list.\n")
	concat = input("Chained concatenation (1 to 7). It corresponds to the maximum number of words that will be concatenate. Must be lower or equal to the number of entry of the input list.\n")
	concat = int(concat)
	cmplx = int(cmplx)
	print("")
	if concat>len(content):
		print("Concatenation value must be lower than the number of entries in the input list")
	else:
		for line in content:
			words = line.split(':')
			if words[0]=="Name":
				tmp = []
				if cmplx>=1:
					tmp.append(words[1].capitalize())
					tmp.append(words[1].lower())
					tmp.append(words[1].upper())
				if cmplx>=2:
					tmp.append(words[1][0].lower())
					tmp.append(words[1][0].upper())
				if cmplx>=3:
					tmp.append(words[1][0:2].lower())
					tmp.append(words[1][0:2].capitalize())
					tmp.append(words[1][0:2].upper())
				if cmplx>=4:
					if len(words[1])>=4:
						tmp.append(words[1][0:3].lower())
						tmp.append(words[1][0:3].capitalize())
						tmp.append(words[1][0:3].upper())
					if len(words[1])>=5:
						tmp.append(words[1][0:4].lower())
						tmp.append(words[1][0:4].capitalize())
						tmp.append(words[1][0:4].upper())
				total.append(tmp)
			if words[0]=="Date":
				if cmplx>=1:
					total.append([words[1][0:2]])
					total.append([words[1][2:4]])
					total.append([words[1][4:8]])
					total.append([words[1][6:8]])
				if cmplx>=2:
					if words[1][2]=='0':
						total.append([words[1][3]])
			if words[0]=="Other":
				if cmplx>=1:
					tmp = []
					tmp.append(words[1].capitalize())
					tmp.append(words[1].lower())
					tmp.append(words[1].upper())
				total.append(tmp)

		#Number of words forecast
		size=len(total)
		perms=1
		nbletters=0
		for i in range(size-concat+1,size+1):
			perms*=i
		tot=0
		for i in total:
			#print i
			for j in i:
				nbletters+=len(j)
			tot+=len(i)
		forecast=((1.000*tot/size)**concat)*perms
		print("Prevision :",int(forecast),"words, for a size of :")

		#Size of the file forecast
		size=(1.000*nbletters/tot)*forecast*concat
		print('%.3f'%((size)/1.0), "Bytes")
		print('%.3f'%((size)/1000.0), "KB")
		print('%.3f'%((size)/1000000.0), "MB")
		print('%.3f'%((size)/1000000000.0), "GB")
		ans=input("\nDo you want to process the Wordlist ? [y]/n\n")
		print("")
		if ans=='y':
		
			#Finding the Permutations and Cartesian Products
			i=1
			nbwords = 0
			for line in list(itertools.permutations(total, concat)):
				print("Progress {:2.1%}".format(1.0 * nbwords / int(forecast)), end="\r")
				#print line
				if concat==1:
					for pw in list(itertools.product(line[0])):
						#print pw[0]
						file.write(pw[0]+"\n")
						nbwords += 1
				if concat==2:
					for pw in list(itertools.product(line[0], line[1])):
						#print pw[0]+pw[1]
						file.write(pw[0]+pw[1]+"\n")
						nbwords += 1
				if concat==3:
					for pw in list(itertools.product(line[0], line[1], line[2])):
						#print pw[0]+pw[1]+pw[2]
						file.write(pw[0]+pw[1]+pw[2]+"\n")
						nbwords += 1
				if concat==4:
					for pw in list(itertools.product(line[0], line[1], line[2], line[3])):
						#print pw[0]+pw[1]+pw[2]+pw[3]
						file.write(pw[0]+pw[1]+pw[2]+pw[3]+"\n")
						nbwords += 1
				if concat==5:
					for pw in list(itertools.product(line[0], line[1], line[2], line[3], line[4])):
						#print pw[0]+pw[1]+pw[2]+pw[3]+pw[4]
						file.write(pw[0]+pw[1]+pw[2]+pw[3]+pw[4]+"\n")
						nbwords += 1
				if concat==6:
					for pw in list(itertools.product(line[0], line[1], line[2], line[3], line[4], line[5])):
						#print pw[0]+pw[1]+pw[2]+pw[3]+pw[4]+pw[5]
						file.write(pw[0]+pw[1]+pw[2]+pw[3]+pw[4]+pw[5]+"\n")
						nbwords += 1
				if concat==7:
					for pw in list(itertools.product(line[0], line[1], line[2], line[3], line[4], line[5], line[6])):
						#print pw[0]+pw[1]+pw[2]+pw[3]+pw[4]+pw[5]+pw[6]
						file.write(pw[0]+pw[1]+pw[2]+pw[3]+pw[4]+pw[5]+pw[6]+"\n")
						nbwords += 1
						
			file.close()
			size = os.path.getsize('Passwords.txt')
			print(nbwords, "words written in Passwords.txt, with size :")
			print('%.3f'%(size), "Bytes")
			print('%.3f'%(size/1000.0), "KB")
			print('%.3f'%(size/1000000.0), "MB")
			print('%.3f'%(size/1000000000.0), "GB")
	

